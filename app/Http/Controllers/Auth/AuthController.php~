<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Session;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    
    /**
     *
     *
    */
    
    public function login()
    {
	return response()->json(Auth::check());
        //return view('auth.login');
    }
    
    /**
     *
     *
    */
    
    public function authentication(Request $request)
    {
        return $this->authenticationCheck($request);
    }
    
    /**
     *
     *
     * @return void
    */

    public function authenticationCheck($request)
    {
        $email = $request->email;
        $password = $request->password;
        $rememberToken = $request->rememberToken;

        if (Auth::attempt(['email' => $email, 'password'=> $password], TRUE)) {

	    Session::save();
            $msg = [
                'status' => Auth::check(),
                'type' => 'AUTH_LOGIN_SUCCESS',
                'msg' => [
                    'title' => trans('msg.login_success_title'),
                    'desc' => trans('msg.login_success_desc'),
	        ],
            ];

            return response()->json($msg);

        } else {
            $msg = [
                'status' => Auth::check(),
                'type' => 'AUTH_LOGIN_FAILED',
                'msg' => [
                    'title' => trans('msg.login_failed_title'),
                    'desc' => trans('msg.login_failed_desc'),
                ]
            ];

            return response()->json($msg, 401);

        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
