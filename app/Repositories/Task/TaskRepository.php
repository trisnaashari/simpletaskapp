<?php

namespace App\Repositories\Task;

use App\Repositories\Task\TaskInterface as TaskInterface;
use App\Task;


class TaskRepository implements TaskInterface
{
    public $task;

    /*
     *
     *
     *
     */
    function __construct(Task $task) {
	$this->task = $task;
    }

    /*
     *
     *
     *
     */
    public function getAll()
    {
        return $this->task->getAll();
    }

    /*
     *
     *
     *
     */
    public function find($id)
    {
        return $this->task->findTask($id);
    }

    /*
     *
     *
     *
     */
    public function delete($id)
    {
        return $this->task->deleteTask($id);
    }
}
