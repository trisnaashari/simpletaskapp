<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Task List</title>

    <!-- Bootstrap CSS File  -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/bootstrap-datepicker3.min.css') }}"/>
    <!-- Jquery JS File -->
    <script src="{{ URL::asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/jquery-1.9.1.min.js') }}"></script>
    <!-- Bootstrap JS File -->
    <script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
    <!-- Bootstrap Datepicker JS File -->
    <script src="{{ URL::asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.date').datepicker({
                format: "yyyy-mm-dd",
                todayHighlight: true
            });
        });
    </script>
</head>
<body>

<div class="container">

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">My Tasks</a>
            </div>
            <ul class="nav navbar-nav">

            </ul>
        </div>
    </nav>

    <head>
        <h1></h1>
    </head>

    @yield('content')

</div>
</body>
</html>
