@extends('layouts.headers.header')

@section('content')

	<div id="wrapper">
      <div class="auth-slider-container text-left">
			<div id="auth-intro">
				<div class="section" style="position: relative;">
					<div class="hidden-xs hidden-sm" 
						style="position: absolute; bottom: 100px; left: 0; right: 0; margin: 0 auto; color: #fff; z-index: 3000; text-align: center;">
						{{ trans('auth.no_account') }}
						&nbsp;
						<a href="{{ url('auth/register') }}" class="load-ajax-content" style="color: #fff">
							<u>{{ trans('auth.register_now') }}</u>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="auth-form-container text-center">
			<div class="auth-helper">
				<div class="auth-box">
					<div class="auth-overlay-bg"></div>
						<h1 class="fs-xs-24 fs-sm-32 fs-md-42 fs-lg-42 fw600">{{ config('settings.app.name') }}</h1>
						<h2 class="fs14 fw100">{{ trans('auth.login_to_continue') }}</strong></h2>
						<form id="form" class="m-t" role="form"
							action="{{ url('auth/login/process') }}" 
							style="margin-top: 50px;">
							<div class="form-group full-width has-feedback">
								<div class="col-xs-12 input-group no-padding">
									<div class="input-group-addon hidden-sm hidden-md hidden-lg">
										<i class="material-icons fs24">account_circle</i>
									</div>
									<input type="text" 
										name="identifier" 
										id="input-auth-identifier" 
										class="form-control input-lg" 
										placeholder="{{ trans('auth.email_or_username') }}" 
										data-error="{{ trans('auth.email_or_username').trans('msg.is_required') }}"
										autocomplete="off"
										required>
										<span class="bar"></span>
										<span class="help-block with-errors pull-right"></span>
								</div>
							</div>
							<div class="form-group full-width has-feedback">
								<div class="col-xs-12 input-group no-padding">
									<div class="input-group-addon hidden-sm hidden-md hidden-lg">
										<i class="material-icons fs24">lock</i>
									</div>
									<input type="password" 
										name="password" 
										id="input-auth-password" 
										class="form-control input-lg" 
										placeholder="{{ trans('auth.password') }}"
										data-error="{{ trans('auth.password') }}?>{{ trans('msg.is_required') }}"
										data-minlength="6"
										autocomplete="off"
										required>
									<span class="bar"></span>
									<span class="help-block with-errors pull-right"></span>
								</div>
							</div>
							<button type="submit" 
								id="btn-auth-login" 
								class="btn btn-primary 
								btn-lg block full-width mt15" 
								data-loading-text="{{ trans('msg.processing') }}">{{ trans('auth.login') }}</button>
							<div class="auth-feedback mt15"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@extends('layouts.footers.footer')
